# Throw and Learn

Throw and Learn was created during a seminar at TU Braunschweig. The AR-Game is based on AR-Code. With this serious game a user is able to learn simple things about programming or serious games itself. 

Processing period: Apr. 2018 - Sep. 2018

## Seminar structure

1. Introduction
2. Gamification
3. AR Core with serious games
4. Development
5. Issues during development
6. Conclusion
7. Literature

## Features 

- Place two bins on a surface detected through the phones kamera
- Throw a paperball into one of the bins to answer the on the top displayed question. 
- A text based tutorial which explains the UI 
- Sounds which are deactivatable in an options pop up

## Serious Game preview

### Game
<img src="res/screenshots/Game.jpg"  width="270" height="555">

### Tutorial
<img src="res/screenshots/Tutorial.jpg"  width="270" height="555">

### Menu
<img src="res/screenshots/Menu.jpg"  width="270" height="555">

