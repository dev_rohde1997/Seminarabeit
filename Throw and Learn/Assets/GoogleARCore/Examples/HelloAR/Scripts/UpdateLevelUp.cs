﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GoogleARCore.Examples.HelloAR
{
    public class UpdateLevelUp : MonoBehaviour
    {
        Text text;
        
        //TIMER VARIABLES 
        DateTime startTime;
        DateTime endTime;
        TimeSpan ts;
        bool timerStarted = false;
        //TIMER VARIABLES

        //LOgger
        ILogger logger = Debug.unityLogger;


        //Animation
        bool animation = false;
        const int STANDARD_FONT_SIZE = 100;

        //Answers
        private const string LEVEL_UP_TXT= "Level up!";
        private void Update()
        {
            endTime = DateTime.Now;
            
            if (ControllerPhase.levelUp)
            {
                animation = true;
                text.text = LEVEL_UP_TXT;
                startTime = DateTime.Now;
                timerStarted = true;
                logger.Log(Constants.LOG_TAG, "Level Up!");
                ControllerPhase.levelUp = false;
            }

            ts = endTime - startTime;
            if (timerStarted && ts.Seconds > 3)
            {
                //Text wieder verschwinden lassen
                logger.Log(Constants.LOG_TAG, "Hide Level Up!");
                text.text = "";
                text.fontSize = STANDARD_FONT_SIZE;
                timerStarted = false;
            }

            if (animation)
            {
                if (text.fontSize >= 150)
                {
                    animation = false;
                }
                text.fontSize++;
            }
        }

        
        void Awake()
        {
            text = GetComponent<Text>();
        }

        

    }
}