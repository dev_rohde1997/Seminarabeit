﻿using System;
using System.Collections;
using System.Linq;
using System.Text;

namespace GoogleARCore.Examples.HelloAR
{
    public class Statements
    {
        private String statement;
        private bool correctness;

        public Statements(string statement, bool correctness)
        {
            this.statement = statement;
            this.correctness = correctness;
        }

        public string Statement
        {
            get
            {
                return statement;
            }

            set
            {
                statement = value;
            }
        }

        public bool Correctness
        {
            get
            {
                return correctness;
            }

            set
            {
                correctness = value;
            }
        }

        public override String ToString()
        {
            return statement + ", " + correctness;
        }
    }
}
