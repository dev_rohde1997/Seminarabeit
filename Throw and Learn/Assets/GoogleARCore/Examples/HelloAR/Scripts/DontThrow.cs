﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GoogleARCore.Examples.HelloAR
{
    /// <summary>
    /// Ingorieren des Touches, wenn ein Button gedrückt wird,
    /// damit Platzierung des Mülleimer nicht auf diesen Touch reagiert
    /// </summary>
    public class DontThrow : MonoBehaviour {

	    public void DontThrowTheBall()
        {
            UI_Manager.ignoreButtonTouch = true;
        }
    }
}

