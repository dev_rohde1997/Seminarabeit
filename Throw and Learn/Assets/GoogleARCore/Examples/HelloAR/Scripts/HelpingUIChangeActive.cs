﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GoogleARCore.Examples.HelloAR
{
    /// <summary>
    /// Hiermit kann die Sichtbarkeit eines AKTIVEN Gameobejets geändert werden
    /// </summary>
    public class HelpingUIChangeActive : MonoBehaviour {
        ILogger logger = Debug.unityLogger;
	    public void ChangeActive()
        {
            if (gameObject.activeSelf)
            {
                gameObject.SetActive(false);
            } else
            {
                gameObject.SetActive(true);
            }
        }
    }
}

