﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GoogleARCore.Examples.HelloAR
{
    /// <summary>
    /// GUI Update von der Hilfsoberfläche
    /// </summary>
    public class UpdateCurrentLevelText : MonoBehaviour {
        Text text;
        int level = (ControllerPhase.playersExperiance / 25);
        ILogger logger = Debug.unityLogger;

        

        private void Update()
        {
            level = (ControllerPhase.playersExperiance / 25);
            text.text = "" + level;
        }
        void Awake()
        {
            text = GetComponent<Text>();
        }
        
        
    }
}

