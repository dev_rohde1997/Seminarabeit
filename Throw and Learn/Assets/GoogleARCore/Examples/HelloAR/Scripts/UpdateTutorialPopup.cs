﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GoogleARCore.Examples.HelloAR
{
    public class UpdateTutorialPopup : MonoBehaviour {

	    // Use this for initialization
	    void Start () {
            if (Tutorial_Manager.SkipTutorialPopup)
            {
                gameObject.SetActive(false);
            }
            else
            {
                gameObject.SetActive(true);
            }
        }
	
	    // Update is called once per frame
	    void Update () {
            //Debug.unityLogger.Log(Constants.LOG_TAG, "UpdateTutorialPopup: " + Tutorial_Manager.ShowTutorialGame);
            if (Tutorial_Manager.ShowTutorialMenu)
            {
                gameObject.SetActive(true);
            } else
            {
                gameObject.SetActive(false);
            }
	    }
    }
}


