﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GoogleARCore.Examples.HelloAR
{
    class Question
    {
        private String question;
        private Statements[] statements;
        private int value;

        ILogger logger = Debug.unityLogger;
        public int Value
        {
            get
            {
                return value;
            }

            set
            {
                //Saving data in PlayerPrefs onChange
                this.value = value;
                logger.Log(Constants.LOG_TAG, "New value for the question: \"" + question + "\" New Value: " + value);
                PlayerPrefs.SetInt(question, value);
                PlayerPrefs.Save();
            }
        }

        public Question(string question, Statements[] statements, int value)
        {
            logger.Log(Constants.LOG_TAG, "Created new Question with:" + question + ", " + statements + ", " + value);
            this.question = question;
            this.statements = statements;
            this.value = value;
        }

        

        /// <summary>
        /// How to get a fully question as a string
        /// </summary>
        /// <returns>The full string with all parts gets returned </returns>
        override public string ToString()
        {
            string s = "";
            s += question;
            
            for (var i = 0; i < statements.Length; i++)
            {
               s += "|\n" + statements[i];
            }
            
            return s;
        }

        public String GetQuestion()
        {
            return question;
        }

        public Stack<Statements> GetStatements()
        {
            Stack<Statements> stack = new Stack<Statements>();
            foreach (var i in statements)
            {
                stack.Push(i);
            }
            return stack;
        }
        
    }
}
