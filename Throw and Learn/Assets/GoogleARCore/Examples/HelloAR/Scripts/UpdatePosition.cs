﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GoogleARCore.Examples.HelloAR
{
    /// <summary>
    /// GUI Update von POS Oberfläche
    /// Oberfläche zur Laufzeitbereitstellung von internen dynamischen Daten
    /// Wurde nur zur Entwicklung benötigt
    /// </summary>
    public class UpdatePosition : MonoBehaviour
    {
        Text text;


        private void Update()
        {
            text.text = "Pos: " + ControllerPhase.pos;
        }




        void Awake()
        {
            text = GetComponent<Text>();
        }
    }
}


