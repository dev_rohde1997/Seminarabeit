﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GoogleARCore.Examples.HelloAR
{
    /// <summary>
    /// Here are all string for the Tutorial Popup.
    /// Also the text of the Tutorial Popus is updated in the Update Method
    /// </summary>
    public class UpdateTutorialText : MonoBehaviour {

        private static string[] PHASES = 
        {
            INTRODUCTION, HOME, THEME_NAME,
            OPTIONS, CURRENT_LEVEL, EXP_PROGRESS, NEXT_LEVEL,
            QUESTION, STATEMENT, PLACE_TOSS, "HIDE1", THROW, "HIDE2", AFTER_HIT
        };
        Text text;
        private const string INTRODUCTION = "Willkommen beim Tutorial zu \nThrow and Learn.\n" +
            "Hier lernst du, wie das Spiel funktioniert.\n" +
            "Wenn du Verstanden hast, was du zu tun hast, drücke auf VERSTANDEN.\n" +
            "Wenn du das Tutorial beenden möchstest hast du dazu jeder Zeit die Möglichkeit,\n" +
            "indem auf das Haus klickst.\n" +
            "Drücke auf VERSTANDEN, um fortzufahren.";

        private const string HOME = "Mit dem Haus kommst du wieder zurück ins Hauptmenü.\n" +
            "Drücke auf VERSTANDEN, um fortzufahren.";

        private const string THEME_NAME = "Hier findest du den Namen des \naktuell gewählten Themas.\n" +
            "Die Fragen, die du beantworten musst, hängen von diesem Thema ab.\n" +
            "Drücke auf VERSTANDEN, um fortzufahren.";

        private const string OPTIONS = "Die Zahnräder öffen die Spieleinstellungen.\n" +
            "Hier kannst du den Sound entweder aktivieren oder auch deaktivieren.\n" +
            "Probiere es gerne aus.\n" +
            "Drücke auf VERSTANDEN, um fortzufahren.";

        private const string CURRENT_LEVEL = "Das ist eine Anzeige für \ndein aktulles Level.\n" +
            "Dein Level steigt mit Erfahrungspunkten auf. Was Erfahrungspunkte sind und wie du " +
            "sie erhältst, erfährst du gleich.\n" +
            "Drücke auf VERSTANDEN, um fortzufahren.";

        private const string EXP_PROGRESS = "Hier findest du die Anzeige," +
            " wie viel Erfahrungspunkte du noch zum nächsten Level brauchst.\n" +
            "Erfahrungspunkte verdienst du, indem du die Fragen richtig geantwortest.\n" +
            "Wenn du alle Aussagen einer Frage richtig beantwortest bekommst du sogar DOPPELTE PUNKTE.\n" +
            "Also streng dich an!\n" +
            "Drücke auf VERSTANDEN, um fortzufahren.";

        private const string NEXT_LEVEL = "Wie du wahrscheinlich schon vermutet hast, " +
            "ist das hier die Anzeige für \ndein nächstes Level.\n" +
            "Drücke auf VERSTANDEN, um fortzufahren.";

        private const string QUESTION = "Hier findest du die aktuelle Frage.\n" +
            "Alle Aussagen, beziehen sich auf die Frage. Was Aussagen sind erfährst du gleich.\n" +
            "Drücke auf VERSTANDEN, um fortzufahren.";

        private const string STATEMENT = "Hier findest du die aktuelle Aussagen.\n" +
            "diese Aussage bezieht sich IMMER auf die Frage darüber.\n" +
            "Du musst nun entscheinden, ob diese Aussage korrekt ode falsch ist.\n" +
            "Wie du das tust erfärhst du gleich.\n" +
            "Drücke auf VERSTANDEN, um fortzufahren.";
        
        private const string PLACE_TOSS = "Um die Fragen beantworten können, " +
            " brauchen wir erstmal zwei Mülleimer. \nNein, keine Echten.\n" +
            " Wir nutzen deine Kamera, um deine Umgebung zu scannen.\n" +
            "Hier werden Ebenen erkannt, auf denen du deine Mülleimer platzieren kannst.\n" +
            "Drücke einfach auf diese Ebenen. Platziere nun die beiden Mülleimer!\n" +
            "Drücke auf VERSTANDEN, um fortzufahren.";
        
        private const string THROW = "SUPER!\n" +
        "Du hast beide Mülleimer platziert.\n Jetzt kann das Spiel beginnen.\n" +
        "Wirf nun den Ball in den grünen Mülleimer!\n" +
        "Drücke auf VERSTANDEN, um fortzufahren.";

        private const string AFTER_HIT = "Herzlichen Glückwunsch.!\n" +
            "Du hast das Tutorial erfolgreich gemeistert!\n" +
            "Du hast nun die Möglichkeit zum Startbildschirm zurückzukehren und ein Thema auszuwählen.\n" +
            "Falls du noch nicht alles verstanden hast, kannst du das Tutorial einfach wiederholen, " +
            "indem du unter der Themenauswahl aus Tutorial drückst.\n" +
            " Wir wünschen dir viel Erfolg und viel Spaß beim Lernen mit THROW AND LEARN!\n" +
            "Drücke auf VERSTANDEN, um zum Startbildschirm zurückzukehren.";

        
        
        // Use this for initialization
        void Start () {
            text = GetComponent<Text>();
        }
	
	    // Update is called once per frame
	    void Update () {
            text.text = PHASES[Tutorial_Manager.CurrPhase];
	    }

        private void Awake()
        {
            text = GetComponent<Text>();
        }


    }

}
