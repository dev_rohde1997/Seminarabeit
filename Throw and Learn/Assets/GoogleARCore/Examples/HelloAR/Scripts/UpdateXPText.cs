﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GoogleARCore.Examples.HelloAR
{
    public class UpdateXPText : MonoBehaviour
    {
        Text text;
        const int STANDARD_FONT_SIZE = 75;
        const string PLUS_XP = "+5XP";
        
        private void Update()
        {
            if (ControllerPhase.showPlusXP)
            {
                text.text = PLUS_XP;
            } else
            {
                text.text = "";
            }
        }
        
        void Awake()
        {
            text = GetComponent<Text>();
        }

        

    }
}