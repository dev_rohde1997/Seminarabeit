﻿//-----------------------------------------------------------------------
// <copyright file="HelloARController.cs" company="Google">
//
// Copyright 2017 Google Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// </copyright>
//-----------------------------------------------------------------------

namespace GoogleARCore.Examples.HelloAR
{
    using System;
    using System.Collections.Generic;
    using GoogleARCore;
    using GoogleARCore.Examples.Common;
    using UnityEngine;

#if UNITY_EDITOR
    // Set up touch input propagation while using Instant Preview in the editor.
    using Input = InstantPreviewInput;
    
#endif

    /// <summary>
    /// Controls the HelloAR example.
    /// </summary>
    public class HelloARController : MonoBehaviour
    {
        /// <summary>
        /// The first-person camera being used to render the passthrough camera image (i.e. AR background).
        /// </summary>
        public Camera FirstPersonCamera;

        /// <summary>
        /// A prefab for tracking and visualizing detected planes.
        /// </summary>
        public GameObject DetectedPlanePrefab;


        /// <summary>
        /// A gameobject parenting UI for displaying the "searching for planes" snackbar.
        /// </summary>
        public GameObject SearchingForPlaneUI;

        /// <summary>
        /// The Prefab of the red, so wrong, papertoss
        /// </summary>
        public GameObject MuellEimerRed;

        /// <summary>
        /// The Prefab of the green, so correct, papertoss
        /// </summary>
        public GameObject MuellEimerGreen;

        /// <summary>
        /// The rotation in degrees need to apply to model when the Andy model is placed.
        /// </summary>
        private const float k_ModelRotation = 180.0f;

        /// <summary>
        /// A list to hold all planes ARCore is tracking in the current frame. This object is used across
        /// the application to avoid per-frame allocations.
        /// </summary>
        private List<DetectedPlane> m_AllPlanes = new List<DetectedPlane>();

        /// <summary>
        /// True if the app is in the process of quitting due to an ARCore connection error, otherwise false.
        /// </summary>
        private bool m_IsQuitting = false;

        /// <summary>
        /// paperball instance
        /// </summary>
        private GameObject paperBall = null;

        /// <summary>
        /// papertoss instance
        /// </summary>
        private GameObject paperTossRED = null;

        /// <summary>
        /// papertoss instance
        /// </summary>
        private GameObject paperTossGREEN = null;
        /// <summary>
        /// Rigidbody of the paperball
        /// </summary>
        private Rigidbody rbPB;

        /// <summary>
        /// List of all existing paperballs
        /// </summary>
        private List<GameObject> paperBallList = new List<GameObject>();

        /// <summary>
        /// List of all existing paperballs, which were thrown into the toss
        /// </summary>
        private List<GameObject> paperBallHitList = new List<GameObject>();

        /// <summary>
        /// Prefab of the paperball
        /// </summary>
        public GameObject PaperBallPrefab;

        /// <summary>
        /// UI Element Popup for in Game Tutorial
        /// </summary>
        public GameObject TutorialGameObject;

        /// <summary>
        /// UI Elementt Popup for Menu Tutorial 
        /// </summary>
        public GameObject TutorialMenuObject;

        /// <summary>
        /// Home Button to go back into menu
        /// </summary>
        public UnityEngine.UI.Button HomeButton;
      


        /// <summary>
        /// Number of placed tosses
        /// </summary>
        private int tossCount = 0;


        private static ILogger logger = Debug.unityLogger;

        public void Init()
        {
            logger.Log(Constants.LOG_TAG, "Start Init");
            if (paperBall != null)
            {
                Destroy(paperBall);
            }
            try
            {

                //Positionierung des Papierballs in Anhänigkeit von der Position und BLickrichtung der Kamera
                paperBall = Instantiate(PaperBallPrefab, new Vector3(Camera.main.transform.position.x,
                    Camera.main.transform.position.y,
                    Camera.main.transform.position.z) + Camera.main.transform.forward * Constants.DISTANCE
                    , Quaternion.identity);
                paperBallList.Add(paperBall);
                rbPB = paperBall.GetComponent<Rigidbody>();
                rbPB.useGravity = false;
                //paperBall.SetActive(false);
                UI_Manager.ignoreButtonTouch = false;

                //TEMP
                //Instantiate(m_müllEimerAndroidPrefab, new Vector3(0,0,0), Quaternion.identity);
            } catch (System.Exception e)
            {
                _ShowAndroidToastMessage("Fehler beim iniziieren" + e);
            }
        }

        private void Start()
        {

            LoadPlayerExperance();
            int isTutorial = -1;
            isTutorial = LoadisTutorialActive();
        }

        private int LoadisTutorialActive()
        {
            //0: Dont Skip, 1 Skip Tutorial
            int isTutorial = 0;
            if (!PlayerPrefs.HasKey("Tutorial"))
            {
                logger.Log(Constants.LOG_TAG, "No Key yet for: Tutorial");
                PlayerPrefs.SetInt("Tutorial", 0);
            }
            isTutorial = PlayerPrefs.GetInt("Tutorial");
            PlayerPrefs.Save();

            logger.Log(Constants.LOG_TAG, "Loaded Show: " + isTutorial);
            if (isTutorial == 1)
            {
                Tutorial_Manager.SkipTutorialPopup = true;
                Tutorial_Manager.ShowTutorialMenu = false;
            }
            else if (isTutorial == 0)
            {
                Tutorial_Manager.SkipTutorialPopup = false;
                Tutorial_Manager.ShowTutorialMenu = true;
            }
            return isTutorial;
        }

        private void LoadPlayerExperance()
        {
            if (!PlayerPrefs.HasKey("EXP"))
            {
                logger.Log(Constants.LOG_TAG, "No EXP Key");
                PlayerPrefs.SetInt("EXP", 0);
            }
            ControllerPhase.playersExperiance = PlayerPrefs.GetInt("EXP");
            logger.Log(Constants.LOG_TAG, "Erfahrungspunkte wurden geladen: " + PlayerPrefs.GetInt("EXP"));
        }

        /// <summary>
        /// The Unity Update() method.
        /// </summary>
        public void Update()
        {
            
            //logger.Log(Constants.LOG_TAG, paperBall.transform.position.ToString());
            _UpdateApplicationLifecycle();
            // Hide snackbar when currently tracking at least one plane.
            SearchPlate();
            if (UI_Manager.UI_is_active)
            {
                _CheckBallPosition();
                return;
            }
            if (ControllerPhase.tossesPlaced == false)
            {
                PlaceToss();
            }
            else if (ControllerPhase.ballThrown == false)
            {
                // logger.Log(Constants.LOG_TAG, "Werfen");
                _TouchInput();
            }
            else
            {
                if (ControllerPhase.hit)
                {
                    //Treffer in den Eimer
                    _RepositionBall();
                    ReactivateGameTutorialPopup();
                    ControllerPhase.hit = false;
                }
                else
                {
                    //Falls Ball nicht von einer Ebene "aufgefangen" wird
                    _CheckBallPosition();
                }
            }

        }

        private void ReactivateGameTutorialPopup()
        {
            if (Tutorial_Manager.ReactivatePopup)
            {
                logger.Log(Constants.LOG_TAG, "Reaktivate Touch Evt");
                Tutorial_Manager.ShowTutorialGame = true;
                Tutorial_Manager.ReactivatePopup = false;
                TutorialGameObject.SetActive(true);
            }
        }

        private void SearchPlate()
        {
            
            Session.GetTrackables<DetectedPlane>(m_AllPlanes);
            bool showSearchingUI = true;
            for (int i = 0; i < m_AllPlanes.Count; i++)
            {
                if (m_AllPlanes[i].TrackingState == TrackingState.Tracking)
                {
                    showSearchingUI = false;
                    break;
                }
            }

            SearchingForPlaneUI.SetActive(showSearchingUI);
        }

        private void PlaceToss()
        {
            _BallInFrontOfCam();
            if (Tutorial_Manager.ShowTutorialGame)
            {
                return;
            }
            Touch touch;
            if (Input.touchCount < 1 || (touch = Input.GetTouch(0)).phase != TouchPhase.Began)
            {
                return;
            }

            // Raycast against the location the player touched to search for planes.
            TrackableHit hit;
            TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinPolygon |
                TrackableHitFlags.FeaturePointWithSurfaceNormal;

            if (Frame.Raycast(touch.position.x, touch.position.y, raycastFilter, out hit))
            {
                // Use hit pose and camera pose to check if hittest is from the
                // back of the plane, if it is, no need to create the anchor.
                if ((hit.Trackable is DetectedPlane) &&
                    Vector3.Dot(FirstPersonCamera.transform.position - hit.Pose.position,
                        hit.Pose.rotation * Vector3.up) < 0)
                {
                    Debug.Log("Hit at back of the current DetectedPlane");
                }
                else
                {
                    AudioManager.playObjectPlayedSound = true;


                    switch (tossCount)
                    {
                        case 0:
                            tossCount++;
                            paperTossGREEN = Instantiate(MuellEimerGreen, hit.Pose.position, hit.Pose.rotation);

                            paperTossGREEN.transform.Rotate(0, k_ModelRotation, 0, Space.Self);

                            var anchor1 = hit.Trackable.CreateAnchor(hit.Pose);

                            paperTossGREEN.transform.parent = anchor1.transform;
                            break;
                        case 1:
                            if (Tutorial_Manager.IsTutorialActive)
                            {
                                Tutorial_Manager.CurrPhase++;
                                Debug.unityLogger.Log(Constants.LOG_TAG, "Phase: " + Tutorial_Manager.CurrPhase);
                                Tutorial_Manager.ShowTutorialGame = true;
                                TutorialGameObject.SetActive(true);
                            }
                            tossCount++;
                            ControllerPhase.tossesPlaced = true;

                            paperTossRED = Instantiate(MuellEimerRed, hit.Pose.position, hit.Pose.rotation);

                            paperTossRED.transform.Rotate(0, k_ModelRotation, 0, Space.Self);

                            var anchor2 = hit.Trackable.CreateAnchor(hit.Pose);

                            paperTossRED.transform.parent = anchor2.transform;

                            //Set Tutorial Popup activ again to continue with tutorial instructions
                            
                            break;
                        default:
                            logger.Log(Constants.LOG_TAG, "Fehler in der Switchanweisung bei der Auswahl vom tossCount: " + tossCount);
                            break;
                    }
                    logger.Log(Constants.LOG_TAG, "Platzieren von noch " + (2 - tossCount) + " Muelleimern");

                    
                    
                     
                }
            }
        }

        /// <summary>
        /// Check and update the application lifecycle.
        /// </summary>
        private void _UpdateApplicationLifecycle()
        {
            // Exit the app when the 'back' button is pressed.
            if (Input.GetKey(KeyCode.Escape))
            {
                Application.Quit();
            }

            // Only allow the screen to sleep when not tracking.
            if (Session.Status != SessionStatus.Tracking)
            {
                const int lostTrackingSleepTimeout = 15;
                Screen.sleepTimeout = lostTrackingSleepTimeout;
            }
            else
            {
                Screen.sleepTimeout = SleepTimeout.NeverSleep;
            }

            if (m_IsQuitting)
            {
                return;
            }

            // Quit if ARCore was unable to connect and give Unity some time for the toast to appear.
            if (Session.Status == SessionStatus.ErrorPermissionNotGranted)
            {
                _ShowAndroidToastMessage("Camera permission is needed to run this application.");
                m_IsQuitting = true;
                Invoke("_DoQuit", 0.5f);
            }
            else if (Session.Status.IsError())
            {
                _ShowAndroidToastMessage("ARCore encountered a problem connecting.  Please start the app again.");
                m_IsQuitting = true;
                Invoke("_DoQuit", 0.5f);
            }
        }

        /// <summary>
        /// Actually quit the application.
        /// </summary>
        private void _DoQuit()
        {
            Application.Quit();
        }

        /// <summary>
        /// Show an Android toast message.
        /// </summary>
        /// <param name="message">Message string to show in the toast.</param>
        private void _ShowAndroidToastMessage(string message)
        {
            AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

            if (unityActivity != null)
            {
                AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
                unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
                {
                    AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity,
                        message, 0);
                    toastObject.Call("show");
                }));
            }
        }

        private void _BallInFrontOfCam()
        {
            rbPB.useGravity = false;
            paperBall.transform.rotation = Quaternion.identity;
            rbPB.velocity = Vector3.zero;
            rbPB.angularVelocity = Vector3.zero;
            paperBall.transform.position = new Vector3(Camera.main.transform.position.x,
                    Camera.main.transform.position.y - Constants.OFFSET, Camera.main.transform.position.z)
                    + Camera.main.transform.forward * Constants.DISTANCE;
        }

        /// <summary>
        /// Handling of touchinput and resulting action
        /// </summary>
        private void _TouchInput()
        {
            //SKIP CONDITIONS DES TOUCHES
            if (ControllerPhase.ballFollowsCam)
            {
                _BallInFrontOfCam();
            }

            if (UI_Manager.ignoreButtonTouch)
            {
                UI_Manager.ignoreButtonTouch = false;
                return;
            }

            if (UI_Manager.skipFirstTouch)
            {
                UI_Manager.skipFirstTouch = false;
                return;
            }
            if (Tutorial_Manager.ShowTutorialGame)
            {
                return;
            }
            //SKIP ENDE
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);

                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        ControllerPhase.startPos = touch.position;
                        ControllerPhase.startTime = System.DateTime.Now;
                        ControllerPhase.skip = false;
                        break;
                    case TouchPhase.Ended:
                        if (ControllerPhase.skip) return;
                        AudioManager.playBallThrownSound = true;
                        ControllerPhase.endTime = System.DateTime.Now;
                        System.TimeSpan ts = ControllerPhase.endTime - ControllerPhase.startTime;
                        ControllerPhase.direction = new Vector3(touch.position.x, touch.position.y, 0) - ControllerPhase.startPos;
                        //Abfangen von Wischgesten nach hinten
                        if (ControllerPhase.direction.y < 0)
                        {
                            return;
                        }
                        
                        //logger.Log(Constants.LOG_TAG, "Animation Go");
                        _Animation(ControllerPhase.direction, ts);
                        ControllerPhase.ballThrown = true;
                        break;
                }
            }

        }

        /// <summary>
        /// Es wird die Animation anhand der übergebenen Ballgeschwindigkeit ausgeführt
        /// Die Gravitation wird aktiviert und eine Kraft wird auf das Papierballobjekt hinzugefuegt
        /// </summary>
        /// <param name="ballThrowingForce">Aktuelle Geschwindigkeit der Kugel</param>
        /// <param name="ballThrowingTime">Dauer des Touchinputs</param>
        private void _Animation(Vector3 ballThrowingForceVector, TimeSpan ballThrowingTime)
        {
            try
            {
                float ballThrowingForceY = _SpeedTimeInfluence(ballThrowingForceVector.y, ballThrowingTime);
                ControllerPhase.ChangeBallFollow();
                rbPB.useGravity = true;
                //Abstandsbestimmung
                ControllerPhase.lastVectorBallTossDistance = _CalcDistanceVectorBallToss(paperTossGREEN);
                ControllerPhase.lastBallTossDistance = (float)_CalcDistanceBallToss(paperTossGREEN);
                
                 rbPB.AddForce(Camera.main.transform.forward * (ballThrowingForceY / Constants.SPEED_REGULATOR_Y)
                    + Camera.main.transform.up * (ballThrowingForceY / Constants.SPEED_REGULATOR_Y)
                    + Camera.main.transform.right * (ballThrowingForceVector.x / Constants.SPEED_REGULATOR_X));
                
                
            } catch(Exception e)
            {
                logger.Log(Constants.LOG_TAG, e);
            }
        }

        /// <summary>
        /// Methode zur Anpassung der Wurfgeschwindigkeit in Abhaenigkeit der Swipe-Geschwindigkeit 
        /// </summary>
        /// <param name="speed">Bisherige Geschwindigkeit</param>
        /// <param name="timespan">Dauer des Touchinputs</param>
        /// <returns>Gibt die regulierte Geschwindigkeit zurueck</returns>
        private float _SpeedTimeInfluence(float speed, TimeSpan timespan)
        {
            speed *= 2;
            float time = (timespan.Seconds * 1000) + timespan.Milliseconds; // Zwischen 100 und 700
            if (time > 700)
            {
                // time = ca 3.5 und groesser
                time /= 200;
            }
            else if (time > 500)
            {
                //time = ca. 2 - 1.428
                time /= 250;
            }
            else if (time > 300)
            {
                //time = 1.6 - 1
                time /= 300;
            }
            else
            {
                //Sonst unverändert
                //_ShowAndroidToastMessage("Speed " + speed);
                return speed;
            }

            speed /= time;

            return speed;
        }

        /// <summary>
        /// Abstandsbestimmung anhand der Koordinaten im R^3 von dem Papierball und dem Papiereimer
        /// </summary>
        /// <returns>Der berechnete Abstand wird zurueckgegeben</returns>
        private Double _CalcDistanceBallToss(GameObject paperToss)
        {
            double dist = 0.0;
            try
            {
                double valX = Math.Pow((paperBall.transform.position.x - paperToss.transform.position.x), 2);
                double valY = Math.Pow((paperBall.transform.position.y - paperToss.transform.position.y), 2);
                double valZ = Math.Pow((paperBall.transform.position.z - paperToss.transform.position.z), 2);

                dist = Math.Abs(Math.Sqrt(valX + valY + valZ));
                ControllerPhase.lastBallTossDistance = (float)dist;
                ControllerPhase.lastVectorBallTossDistance = new Vector3((float)valX, (float)valY, (float)valZ);
                
            } catch(Exception e)
            {
                logger.Log(Constants.LOG_TAG, e);
               
            }
            return dist;
        }

        private Vector3 _CalcDistanceVectorBallToss(GameObject paperToss)
        {
            if (paperBall == null || paperToss == null)
            {
                return Vector3.zero;
            }
            double valX = Math.Pow((paperBall.transform.position.x - paperToss.transform.position.x), 2);
            double valY = Math.Pow((paperBall.transform.position.y - paperToss.transform.position.y), 2);
            double valZ = Math.Pow((paperBall.transform.position.z - paperToss.transform.position.z), 2);

            return new Vector3((float)valX, (float)valY, (float)valZ);
        }

        private void _CheckBallPosition()
        {
            if (paperBall.transform.position.y < paperTossGREEN.transform.position.y + Constants.RESET_HIGH)
            {
                _RepositionBall();
            }
        }

        /// <summary>
        /// Zureucksetzung des Papierball vor die Kamera
        /// </summary>
        public void _RepositionBall()
        {
            ControllerPhase.ballThrown = false;
            ControllerPhase.ballFollowsCam = true;

            _BallInFrontOfCam();
        }

        public void OnApplicationPause(bool pause)
        {
            
            if (pause)
            {
                logger.Log(Constants.LOG_TAG, "Die Anwendung wurde pausiert!");
                _SaveData();
                ControllerPhase.pause = true;
                HomeButton.onClick.Invoke();
            }
            else
            {
                logger.Log(Constants.LOG_TAG, "Die Anwendung wurde fortgesetzt!");
                
            }
        }

        /// <summary>
        /// Speichern der Daten vor Beendigung der Application
        /// </summary>
        private void OnApplicationQuit()
        {
            logger.Log(Constants.LOG_TAG, "Die Anwendung wird beendet");
            _SaveData();
        }

        /// <summary>
        /// Speichern der Daten vor Pausierung der Application
        /// </summary>
        private void _SaveData()
        {
            logger.Log(Constants.LOG_TAG, "Daten werden gespeichert");

            PlayerPrefs.SetInt("EXP", ControllerPhase.playersExperiance);
            Question[] questions = QuestionManager.questions;
            for (int i = 0; i < questions.Length; i++)
            {
                string current_question = questions[i].ToString();
                string[] questionFull = current_question.Split('|');
                string key = questionFull[0] + "|" + questionFull[1]; //Frage + Antwort

                logger.Log(Constants.LOG_TAG, "Der Key zum speichern der Frage: " + key + " :: Mit dem Value: " + questionFull[2]);
                PlayerPrefs.SetInt(key, Int32.Parse(questionFull[2]));
            }


            PlayerPrefs.SetInt("Tutorial", Tutorial_Manager.SkipTutorialPopup ? 1 : 0);
            //Reset
            //PlayerPrefs.SetInt("Tutorial", 0);

            logger.Log(Constants.LOG_TAG, "Tutorial active :"  +  Tutorial_Manager.SkipTutorialPopup + " gespeichert.");
            PlayerPrefs.Save();
        }

        private void ResetTossCount()
        {
            tossCount = 0;
        }

        private void DestroyObjects()
        {
            Destroy(paperTossGREEN);
            Destroy(paperTossRED);
        }

        public void Reset()
        {
            TutorialGameObject.SetActive(false);
            ResetTossCount();
            DestroyObjects();
        }
    }
}
