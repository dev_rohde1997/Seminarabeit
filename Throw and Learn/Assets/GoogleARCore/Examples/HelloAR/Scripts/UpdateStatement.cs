﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GoogleARCore.Examples.HelloAR
{
    public class UpdateStatement : MonoBehaviour
    {
        Text text;
        
        
        private void Update()
        {
            Statements s = QuestionManager.statementStack.Peek();
            text.text = s.Statement;

        }
        
        void Awake()
        {
            text = GetComponent<Text>();
        }

        

    }
}