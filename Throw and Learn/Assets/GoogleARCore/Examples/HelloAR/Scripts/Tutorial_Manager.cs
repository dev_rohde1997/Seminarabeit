﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GoogleARCore.Examples.HelloAR
{
    public class Tutorial_Manager : MonoBehaviour
    {

        public GameObject StartPanel;

        public GameObject GamePanel;

        public Button HomeButton;

        public GameObject TutorialPopup;
        
        /// <summary>
        /// String, witch contains the current phase of the tutorial
        /// </summary>
        public static string phase;

        private ILogger logger = Debug.unityLogger;
        /// <summary>
        /// Getter and Setter for
        /// <see cref="phase"/>
        /// </summary>
        public string Phase
        {
            get
            {
                return phase;
            }

            set
            {
                phase = value;
            }
        }
        
        public static int CurrPhase = 0;
        public void SetPhaseTutorial(string phase)
        {
            this.Phase = phase;
        }

        /// <summary>
        /// Tutorial should only shown once
        /// </summary>
        private static bool showTutorialMenu = false;

        /// <summary>
        /// Getter and Setter for
        /// <see cref="showTutorialMenu"/>
        /// </summary>
        public static bool ShowTutorialMenu
        {
            get
            {
                return showTutorialMenu;
            }

            set
            {
                showTutorialMenu = value;
            }
        }

        /// <summary>
        /// Tutorial isActive in the game UI
        /// </summary>
        private static bool showTutorialGame = false;

        /// <summary>
        /// Getter and Setter for
        /// <see cref="showTutorialGame"/>
        /// </summary>
        public static bool ShowTutorialGame
        {
            get
            {
                return showTutorialGame;
            }

            set
            {
                showTutorialGame = value;
            }
        }

        /// <summary>
        /// Getter and Setter for 
        /// <see cref="isTutorialActive"/>
        /// </summary>
        public static bool IsTutorialActive
        {
            get
            {
                return isTutorialActive;
            }

            set
            {
                isTutorialActive = value;
            }
        }

        public static bool ReactivatePopup
        {
            get
            {
                return reactivatePopup;
            }

            set
            {
                reactivatePopup = value;
            }
        }

        public static bool SkipTutorialPopup
        {
            get
            {
                return skipTutorialPopup;
            }

            set
            {
                skipTutorialPopup = value;
            }
        }

        /// <summary>
        /// When the user plays the Tutorial and the popup is open. 
        /// There should be all throwing touches ignored.
        /// </summary>
        private static bool isTutorialActive = false;

        /// <summary>
        /// Var for reactivating the active beeing of the Tutorial Popup in Game
        /// </summary>
        private static bool reactivatePopup = false;

        /// <summary>
        /// Loading of the key, if the Tutorial has been already played or skipped
        /// </summary>
        private static bool skipTutorialPopup = false;

        public void StartTutorial()
        {
            try
            {
                Debug.unityLogger.Log(Constants.LOG_TAG, "StartTutorial");
                ShowTutorialGame = true;
                TutorialPopup.SetActive(true);
                IsTutorialActive = true;
                ShowTutorialMenu = false;
                CurrPhase = 0;
                Debug.unityLogger.Log(Constants.LOG_TAG, TutorialPopup.activeSelf);
            }
            catch (Exception e)
            {
                Debug.unityLogger.Log(Constants.LOG_TAG, "Fehler in StartTutorial");
                Debug.unityLogger.Log(Constants.LOG_TAG, e);

                throw;
            }
        }

        public void Next()
        {
            CurrPhase++;
            switch (CurrPhase)
            {
                
                case 10 :
                    //Papertosses have to be placed

                    Tutorial_Manager.ShowTutorialGame = false;
                    break;
                case 12 :
                    //Hiding Popup, cause Call should be thrown;
                    Tutorial_Manager.ShowTutorialGame = false;
                    break;
                case 14 :
                    //Closing Tutorial and continue in menu
                    Tutorial_Manager.IsTutorialActive = false;
                    Tutorial_Manager.ShowTutorialGame = false;
                    HomeButton.onClick.Invoke();
                    break;
               
            }
        }

        public void TurnOffTutorialValue(Toggle toggle)
        {
            int val = 0;

            Debug.unityLogger.Log(Constants.LOG_TAG, "Toggle: " + toggle.isOn);
            
            showTutorialMenu = false;
            if (toggle.isOn)
            {
                skipTutorialPopup = true;
                val = 1;
            } else
            {
                skipTutorialPopup = false;
            }
            Debug.unityLogger.Log(Constants.LOG_TAG, "Toggle: " + skipTutorialPopup);
            PlayerPrefs.SetInt("Tutorial", val);
            PlayerPrefs.Save();
        }

        public static void Reset()
        {
            CurrPhase = 0;
            ShowTutorialGame = false;
            isTutorialActive = false;
        }

       
        

    }
}