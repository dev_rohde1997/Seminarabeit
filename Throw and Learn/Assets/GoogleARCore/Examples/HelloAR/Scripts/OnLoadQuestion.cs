﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace GoogleARCore.Examples.HelloAR
{
    public class OnLoadQuestion : MonoBehaviour {

        Text text;
        // Update is called once per frame
        private static ILogger logger = Debug.unityLogger;
        Question current_question;
        
        void Update () {
            current_question = QuestionManager.questions[QuestionManager.current_question_index];
            text.text = "" + QuestionManager.questions[QuestionManager.current_question_index].GetQuestion();
        }

        void Awake()
        {
            text = GetComponent<Text>();
        }

        // Update is called once per frame
        Question[] Add(Question q)
        {
            Question[] temp = new Question[QuestionManager.questions.Length + 1];
            for (int i = 0; i < QuestionManager.questions.Length; i++)
            {
                temp[i] = QuestionManager.questions[i];
            }
            temp[temp.Length - 1] = q;
            return temp;
        }

        
        /// <summary>
        /// Laden der Fragen je nach ausgewähltem Thema
        /// Speichern der einzelenen Aussagen in einem Stack
        /// </summary>
        public void LoadQuestions()
        {
            try
            {
                Stack<Statements> stack = new Stack<Statements>();
                //Index of the subject
                int index = ThemeManager.GetThemeID();
                //Load theme specific multiple choise questions
                //Alle Fragen
                String[] questionSet = QuestionList.GetQuestion(index);
                string question;
                for (int i = 0; i < questionSet.Length; i++)
                {
                    //logger.Log(Constants.LOG_TAG, questionSet[i]);
                    //Einzelne Fragen mit der Antwort
                    string[] multipleChoise = questionSet[i].Split('|');
                    question = multipleChoise[0];
                    
                    for (var j = 0; j < multipleChoise.Length; j++)
                    {
                        
                        if (j == 0) continue;
                        stack.Push(new Statements(multipleChoise[j], Boolean.Parse(multipleChoise[j+1])));
                        j++;
                    }

                    if (!PlayerPrefs.HasKey(question))
                    {
                        PlayerPrefs.SetInt(question, 0);
                    }
                    int value = PlayerPrefs.GetInt(question);
                    PlayerPrefs.Save();
                    

                    QuestionManager.questions = Add(new Question(question, stack.ToArray(), value));
                    //logger.Log(Constants.LOG_TAG, new Question(question, stack.ToArray()));
                    stack.Clear();
                }

                QuestionManager.NewQuestion();

            }
            catch(Exception e)
            {
                logger.Log(Constants.LOG_TAG, e);
            }
            
        }

    }
}

