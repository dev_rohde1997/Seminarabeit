﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GoogleARCore.Examples.HelloAR
{
    /// <summary>
    /// Statische Klasse mit allen Variablen, die Klassenübergreifend gebraucht werden
    /// <see cref="HelloARController"/>
    /// <see cref="OnCollide"/>
    /// <see cref="OnTrigger"/>
    /// </summary>
    class ControllerPhase
    {
        //TODO: Comment and order AUFRAEUMEN!!!


        public static bool skip = true;
        //Soll der Ball in der Mitte des Bildschirms gehalten werden
        public static bool ballFollowsCam = true;
        //Befindet sich der Ball gerade in einer Wurfanimation
        public static bool ballThrown = false;
        //Wurden die Papiereimer platziert
        public static bool tossesPlaced = false;
        //Wurde die Triggerzone innerhalb eines Papierkorbs getroffen
        public static bool hit = false;
        public static float lastBallTossDistance = 0;
        public static Vector3 lastVectorBallTossDistance = Vector3.zero;
        
        public static bool pause = false;
        public static float paperTossY = 0.0f;

        //TEMP - Hilfsanzeige für Daten
        public static String pos = "";

        //Farben für die Ebene
        public static Color[] m_planeColors = new Color[] {
            new Color(1.0f, 1.0f, 1.0f),
            new Color(0.956f, 0.262f, 0.211f),
            new Color(0.913f, 0.117f, 0.388f),
            new Color(0.611f, 0.152f, 0.654f),
            new Color(0.403f, 0.227f, 0.717f),
            new Color(0.247f, 0.317f, 0.709f),
            new Color(0.129f, 0.588f, 0.952f),
            new Color(0.011f, 0.662f, 0.956f),
            new Color(0f, 0.737f, 0.831f),
            new Color(0f, 0.588f, 0.533f),
            new Color(0.298f, 0.686f, 0.313f),
            new Color(0.545f, 0.764f, 0.290f),
            new Color(0.803f, 0.862f, 0.223f),
            new Color(1.0f, 0.921f, 0.231f),
            new Color(1.0f, 0.756f, 0.027f)
        };
        //Wurfrichtungen
        public static Vector3 startPos = Vector3.zero;
        public static Vector3 direction = Vector3.zero;
        //Zeiten
        public static DateTime startTime = new DateTime();
        public static DateTime endTime = new DateTime();


        //LOGGER
        private static ILogger logger = Debug.unityLogger;

        //Player Erfahrungspunkte
        public static int playersExperiance = 0;

        //Streak of correct answers
        //As of a streak of 3, the player gehts double experiance
        
        //Hide plus XP Text
        public static bool showPlusXP = false;

        //Level Up Event
        public static bool levelUp = false;


        /// <summary>
        /// Ändert das Folgen des Balles der Kamera
        /// </summary>
        public static void ChangeBallFollow()
        {
            if(ballFollowsCam)
            {
                ballFollowsCam = false;
            } else
            {
                ballFollowsCam = true;
            }
        }
        /// <summary>
        /// (Methode von HelloAR)
        /// Erstellt einen Toast der Länge 0
        /// </summary>
        /// <param name="message"></param>
        public static void ShowAndroidToastMessage(string message)
        {
            AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

            if (unityActivity != null)
            {
                AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
                unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
                {
                    AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity,
                        message, 0);
                    toastObject.Call("show");
                }));
            }
        }

        public static void Reset()
        {
            skip = true;
            //Soll der Ball in der Mitte des Bildschirms gehalten werden
            ballFollowsCam = true;
            //Befindet sich der Ball gerade in einer Wurfanimation
             ballThrown = false;
            //Wurden die Papiereimer platziert
            tossesPlaced = false;
            //Wurde die Triggerzone innerhalb eines Papierkorbs getroffen
            hit = false;
            lastBallTossDistance = 0;
            lastVectorBallTossDistance = Vector3.zero;
            pause = false;
            paperTossY = 0.0f;
            //TEMP - Hilfsanzeige für Daten
            pos = "";
            //Wurfrichtungen
            startPos = Vector3.zero;
            direction = Vector3.zero;
            //Zeiten
            startTime = new DateTime();
            endTime = new DateTime();
      }

    }
}
