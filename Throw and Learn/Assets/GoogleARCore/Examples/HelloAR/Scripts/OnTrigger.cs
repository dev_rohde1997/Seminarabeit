﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GoogleARCore.Examples.HelloAR
{
    /// <summary>
    /// Reaction to the Triggerzone inside of the Papertoss
    /// </summary>
    public class OnTrigger : MonoBehaviour {

        private static ILogger logger = Debug.unityLogger;
        private static bool allCorrect = true;
        private static int extraEXP = 0;
        

        /// <summary>
        /// Mehthod triggered if any object (with a physical body) enters the Triggerzone
        /// This method handles the answer checking and what has to happen next
        /// </summary>
        /// <param name="other">Object of the collision</param>
        private void OnTriggerEnter(Collider other)

        {
            try
            {
                bool answer = QuestionManager.statementStack.Peek().Correctness;
                if (this.gameObject.name.Equals("Muell_Green(Clone)")) {
                    if (answer == true)
                    {
                        CorrecrtAnswer();
                    } else if (answer == false)
                    {
                        WrongAnswer();
                    }
                } else if (this.gameObject.name.Equals("Muell_Red(Clone)"))
                {
                    if (answer == true)
                    {
                        WrongAnswer();
                    }
                    else if (answer == false)
                    {
                        CorrecrtAnswer();
                    }
                }
                ControllerPhase.hit = true;
            }
            catch (Exception e)
            {
                logger.Log(Constants.LOG_TAG, e);
                throw e;
            }

        }

        /// <summary>
        /// Case the statement got answered correct
        /// </summary>
        private void CorrecrtAnswer()
        {
            try
            {

                AudioManager.playCorrectAnswerSound = true;
                UI_Manager.answerCorrect = true;
                if (Tutorial_Manager.IsTutorialActive)
                {
                    Tutorial_Manager.CurrPhase++;
                    Debug.unityLogger.Log(Constants.LOG_TAG, "Phase: " + Tutorial_Manager.CurrPhase);
                    Tutorial_Manager.ReactivatePopup = true;
                }
                //Ein EXP für eine korrekte Antwort
                OwnExperiance(1);
                extraEXP++;
                ContinueAfterAnswerEvaluation();
            }
            catch (Exception e)
            {
                logger.Log(Constants.LOG_TAG, e);
            }
        }

        /// <summary>
        /// Getting the Player experiance points for each correct answer
        /// </summary>
        /// <param name="exp">Number of raising EXP</param>
        private static void OwnExperiance(int exp)
        {
            try
            {
                int oldLevel = ControllerPhase.playersExperiance / 25;
                ControllerPhase.playersExperiance += exp;
                int newLevel = ControllerPhase.playersExperiance / 25;
                if (oldLevel != newLevel)
                {
                    ControllerPhase.levelUp = true;
                    AudioManager.playLevelUpSound = true;
                }
            }
            catch (Exception e)
            {
                logger.Log(Constants.LOG_TAG, e);
            }
        }

        /// <summary>
        /// Case the statement got answered wrong
        /// </summary>
        private void WrongAnswer()
        {
            try
            {
                allCorrect = false;
                AudioManager.playWrongAnswerSound = true;
                UI_Manager.answerWrong = true;
                ContinueAfterAnswerEvaluation();
            }
            catch (Exception e)
            {
                logger.Log(Constants.LOG_TAG, e);
            }
        }

        /// <summary>
        /// After answering the question
        /// Select the next statement or 
        /// calculating a new question
        /// </summary>
        private void ContinueAfterAnswerEvaluation()
        {
            try
            {
                QuestionManager.statementStack.Pop();
                if (QuestionManager.statementStack.Count == 0)
                {

                    //If all answers correct, the player gets extra XP
                    if (allCorrect)
                    {
                        //Expend value of correct answers
                        QuestionManager.questions[QuestionManager.current_question_index].Value++;
                        //Double EXP for answering fully correct
                        logger.Log(Constants.LOG_TAG, "Extra XP: " + extraEXP);
                        OwnExperiance(extraEXP);
                        //Reset Extra Points
                        extraEXP = 0;
                    }
                    QuestionManager.NewQuestion();
                    allCorrect = true;
                }


            }
            catch (Exception e)
            {
                logger.Log(Constants.LOG_TAG, e);
            } 
        }


    }


}

