﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoogleARCore.Examples.HelloAR
{
    /// <summary>
    /// Sammlung der Konstanten für das Spiel
    /// </summary>
    class Constants
    {
        //Distance zur Kamera Y
        public const float DISTANCE = 0.6f;
        //Distance zur Kamera Z
        public const float OFFSET = 0.2f;
        //Höhe bis der Ball zurückgesetzt wird
        public const float RESET_HIGH = -5f;
        //Reglulierung der Y Geschwindigkeit
        public const float SPEED_REGULATOR_Y= 9.0f;
        //Reglulierung der X Geschwindigkeit
        public const float SPEED_REGULATOR_X = 5.0f;
        //Mindest Gesamtabstand, damit es Punkte gibt
        public const float MIN_DISTANCE = 0.5f;
        //Mindest Gesamtabstand X + Z, damit es Punkte gibt
        public const float MIN_XZ_DISTANCE = 0.3f;
        //Multiplikator für die Punkte
        public const float POINT_MULTIPLIER = 3f;
        

        //Tag fuer dedn Log
        public static string LOG_TAG = "LearnShot";

    }
}
