﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GoogleARCore.Examples.HelloAR
{
    public class ThemeManager : MonoBehaviour
    {
        //INDEX OF THEMES
        public const int INDEX_WISSENSCHAFTLICHE_LITERATUR = 1;
        public const int INDEX_INTERNETRECHERCHE = 2;
        public const int INDEX_PROGRAMMIEREN_1 = 3;
        public const int INDEX_TUTORIAL = 4;

        //NAMES OF THEMES
        public const string WISSENSCHAFTLICHE_LITERATUR = "Wissenschaftliche Literatur erkennen";
        public const string INTERNETRECHERCHE = "Internetrecherche";
        public const string PROGRAMMIEREN_1 = "Programmieren 1";
        public const string TUTORIAL = "Tutorial";


        private static int themeID = 0;

        
        public static int GetThemeID()
        {
            switch(UI_Manager.theme)
            {
                case WISSENSCHAFTLICHE_LITERATUR:
                    themeID = INDEX_WISSENSCHAFTLICHE_LITERATUR;
                    break;
                case INTERNETRECHERCHE:
                    themeID = INDEX_INTERNETRECHERCHE;
                    break;
                case PROGRAMMIEREN_1:
                    themeID = INDEX_PROGRAMMIEREN_1;
                    break;
                case TUTORIAL:
                    themeID = INDEX_TUTORIAL;
                    break;
            }
            Debug.unityLogger.Log(Constants.LOG_TAG, "Chose question id: " + themeID);
            return themeID;
        }

        public static void ThemeReset()
        {
            themeID = 0;
        }

        
    }
}

