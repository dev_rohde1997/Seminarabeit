﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GoogleARCore.Examples.HelloAR
{
    /// <summary>
    /// GUI Update von der Hilfsoberfläche
    /// </summary>
    public class UpdateNextLevelText : MonoBehaviour {
        Text text;
        int level = (ControllerPhase.playersExperiance / 25);
        ILogger logger = Debug.unityLogger;

        
        private void Update()
        {
            level = (ControllerPhase.playersExperiance / 25);
            text.text = "" + (level + 1);
        }
        
        void Awake()
        {
            text = GetComponent<Text>();
        }
        
        
    }
}

