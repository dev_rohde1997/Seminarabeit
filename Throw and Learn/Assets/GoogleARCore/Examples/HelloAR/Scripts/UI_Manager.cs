﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GoogleARCore.Examples.HelloAR
{ 
    public class UI_Manager : MonoBehaviour {

        
        //Generell if UI is active
        public static bool UI_is_active = true;
        //Show or hide Option Panel
        public static bool options_is_active = false;
        //Show or hide Menu Panel
        public static bool menu_is_active = false;
        //Show or hide Chose Panel
        public static bool choser_is_active = false;

        //Ignore Touch for thworing or placing, when touch just used for UI interaction
        public static bool ignoreButtonTouch = true;
        public static bool skipFirstTouch = true;

        //ACK Update
        public static bool answerCorrect = false;
        public static bool answerWrong = false;
        /// <summary>
        /// Setter for UI_isActive
        /// </summary>
        /// 
        public static string theme = "";



        /// <summary>
        /// UI_isActive turns off.
        /// Methods is triggers from UI Button
        /// </summary>
        public void Set_UI_OFF()
        {
            UI_is_active = false;
        }

        /// <summary>
        /// UI_isActive turns on.
        /// Methods is triggers from UI Button
        /// </summary>   
        public void Set_UI_ON()
        {
            UI_is_active = true;
        }

        
        public static void IgnoreButtonTouch()
        {
            ignoreButtonTouch = false;
        }

        public void SetTheme(string t)
        {
            theme = t;
        }

        public void ChangeUIActive()
        {
            if (UI_is_active)
            {
                Set_UI_OFF();
            } else
            {
                Set_UI_ON();
            }
        }

        /// <summary>
        /// Resets all important vars to the default settings
        /// </summary>
        public static void Reset()
        {
            UI_is_active = true;
            options_is_active = false;
            menu_is_active = false;
            choser_is_active = false;ignoreButtonTouch = true;
            skipFirstTouch = true;answerCorrect = false;
            answerWrong = false;
            theme = "";
        }





    }

}
