﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GoogleARCore.Examples.HelloAR
{
    public class UpdateTutorialGamePopup : MonoBehaviour {

        public RectTransform rectTransform;
	    // Use this for initialization
	    void Start () {
            if (Tutorial_Manager.ShowTutorialGame)
            {
                gameObject.SetActive(true);
            }
            else
            {
                gameObject.SetActive(false);
            }
        }
	
	    // Update is called once per frame
	    void Update () {
            //Show or hide
            Debug.unityLogger.Log(Constants.LOG_TAG, "Show TutGamePop: " + Tutorial_Manager.ShowTutorialGame + " Position: " + this.gameObject.transform.position);
            if (Tutorial_Manager.ShowTutorialGame)
            {
                gameObject.SetActive(true);
            } else
            {
                gameObject.SetActive(false);
            }

            //Update position
            if (Tutorial_Manager.CurrPhase < 7)
            {
                //position is tracket from bottom left x, y, z
                //rectTransform.position = new Vector3(540,1253, 0);
                //rectTransform.position = new Vector3(0, 143, 0);
                rectTransform.position = new Vector3(360, 835.3f, 0);
            }
            else
            {
                //rectTransform.position = new Vector3(540, 725.0f, 0);
                //rectTransform.position = new Vector3(0, -385.0f, 0);
                rectTransform.position = new Vector3(369, 450, 0);

            }
        }
    }
}


