﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GoogleARCore.Examples.HelloAR
{
    public class UpdateUIOptions : MonoBehaviour {
        
	
	    // Update is called once per frame
	    void Update () {
		    if(UI_Manager.options_is_active)
            {
                gameObject.SetActive(true);
            } else
            {
                gameObject.SetActive(false);
            }
        }
    }
}

