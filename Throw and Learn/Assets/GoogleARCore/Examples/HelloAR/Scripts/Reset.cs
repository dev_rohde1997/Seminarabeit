﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GoogleARCore.Examples.HelloAR
{ 
    /// <summary>
    /// Zurücksetzen aller Werte aus <see cref="ControllerPhase"/>
    /// </summary>
public class Reset : MonoBehaviour {

        

        public void ResetVariables()
        {
            
            ControllerPhase.Reset();
            UI_Manager.Reset();
            QuestionManager.Reset();
            Tutorial_Manager.Reset();
    }
        
    }
}

