﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GoogleARCore.Examples.HelloAR
{
    public class AudioManager : MonoBehaviour {

    
    public static bool SoundsActivce = true;

    //Manage sound variables
    public static bool playCorrectAnswerSound = false;
    public static bool playWrongAnswerSound = false;
    public static bool playObjectPlayedSound = false;
    public static bool playBallThrownSound = false;
    public static bool playLevelUpSound = false;

        //LOGGER
        ILogger logger = Debug.unityLogger;
        void Start()
        {
            if (!PlayerPrefs.HasKey("Sound"))
            {
                logger.Log(Constants.LOG_TAG, "Sound ist nicht gespeichert");
                PlayerPrefs.SetInt("Sound", 0);
            }
            if (PlayerPrefs.GetInt("Sound") == 0)
            {
                SoundsActivce = false;
            } else
            {
                SoundsActivce = true;
            }
            logger.Log(Constants.LOG_TAG, "Sound geladen mit: " + PlayerPrefs.GetInt("Sound"));

            PlayerPrefs.Save();
        }

        private void OnApplicationPause(bool pause)
        {
            if (pause)
            {
                int value;
                if (SoundsActivce)
                {
                    value = 1;
                } else
                {
                    value = 0;
                }
                logger.Log(Constants.LOG_TAG, "Sound speichern mit: " + value);
                PlayerPrefs.SetInt("Sound", value);
                PlayerPrefs.Save();
            } 
        }

        private void OnApplicationQuit()
        {
            int value;
            if (SoundsActivce)
            {
                value = 1;
            }
            else
            {
                value = 0;
            }
            logger.Log(Constants.LOG_TAG, "Sound speichern mit: " + value);
            PlayerPrefs.SetInt("Sound", value);
            PlayerPrefs.Save();
        }

        public void ChangeSoundOnOff()
        {
            if (SoundsActivce)
            {
                SoundsActivce = false;
                logger.Log(Constants.LOG_TAG, "Music ist now off");
            } else
            {
                SoundsActivce = true;
                logger.Log(Constants.LOG_TAG, "Music ist now on");
            }
            
            

        }
    }
}

