﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GoogleARCore.Examples.HelloAR
{
    public class UpdateACKText : MonoBehaviour
    {
        Text text;
        
        //TIMER VARIABLES 
        DateTime startTime;
        DateTime endTime;
        TimeSpan ts;
        bool timerStarted = false;
        //TIMER VARIABLES

        //LOgger
        ILogger logger = Debug.unityLogger;


        //Animation
        bool animation = false;
        const int STANDARD_FONT_SIZE = 75;

        //Answers
        private const string CORRECT_ANSEWR = "Korrekt!";
        private const string WRONG_ANSEWR = "Falsch!";
        private void Update()
        {
            endTime = DateTime.Now;
            ts = endTime - startTime;
            if (UI_Manager.answerCorrect)
            {
                ShowAndChangeText(Color.green, CORRECT_ANSEWR);
                UI_Manager.answerCorrect = false;
            }

            if (UI_Manager.answerWrong)
            {
                ShowAndChangeText(Color.red, WRONG_ANSEWR);
                UI_Manager.answerWrong = false;
            }
            ts = endTime - startTime;
            if (timerStarted && ts.Seconds > 2)
            {
                //Text wieder verschwinden lassen
                text.text = "";
                timerStarted = false;
                text.fontSize = STANDARD_FONT_SIZE;
                ControllerPhase.showPlusXP = false;
            }

            if (animation)
            {
                if (text.fontSize >= 120)
                {
                    animation = false;
                }
                text.fontSize++;
                text.fontSize++;
                text.fontSize++;
            }
        }

        private void ShowAndChangeText(Color color, string txt)
        {
            text.color = color;
            text.text = txt;
            startTime = DateTime.Now;
            timerStarted = true;
            animation = true;
            if (txt.Equals(CORRECT_ANSEWR))
                ControllerPhase.showPlusXP = true;
        }

        void Awake()
        {
            text = GetComponent<Text>();
        }

        

    }
}