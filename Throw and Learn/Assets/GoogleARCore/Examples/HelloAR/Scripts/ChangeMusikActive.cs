﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GoogleARCore.Examples.HelloAR
{
    public class ChangeMusikActive : MonoBehaviour {

        Text text;
        
        private void Start()
        {
            text = GetComponent<Text>();
        }

        private void Update()
        {
            if (AudioManager.SoundsActivce)
            {
                text.text = "AN";

            }
            else
            {
                text.text = "AUS";
            }
        }

        void Awake()
        {
            text = GetComponent<Text>();
        }
    }
}

