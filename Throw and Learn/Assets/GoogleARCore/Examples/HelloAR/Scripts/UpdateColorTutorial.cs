﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

namespace GoogleARCore.Examples.HelloAR
{
    
    public class UpdateColorTutorial : MonoBehaviour {
        private static ILogger logger = Debug.unityLogger;

        //UI Objects
        public GameObject Home;
        private Image imageHome;


        public GameObject Theme;
        private Image imageTheme;

        public GameObject Options;
        private Image imageOptions;

        public GameObject Curr_Level;
        private Image imageCurr_Level;

        public GameObject Level_Progress;
        private Image imageLevel_Progress;

        public GameObject Level_Progess_Fill;
        private Image imageProgess_Fill;

        public GameObject Next_Level;
        private Image imageNext_Level;

        public GameObject Question;
        private Image imageQuestion;

        public GameObject Statement;
        private Image imageStatement;
        //UI End

        //Colors

        static Color blue = ConvertColor(2, 100, 187, 255);
        static Color lightblue = ConvertColor(107, 165, 223, 255);
        static Color transparent = ConvertColor(255, 255, 255, 0);
        //Bools
        static bool increase = true;

        
        private void Start()
        {
            imageHome = Home.GetComponent<Image>();
            imageTheme = Theme.GetComponent<Image>();
            imageOptions = Options.GetComponent<Image>();
            imageCurr_Level = Curr_Level.GetComponent<Image>();
            imageLevel_Progress = Level_Progress.GetComponent<Image>();
            imageProgess_Fill = Level_Progess_Fill.GetComponent<Image>();
            imageNext_Level = Next_Level.GetComponent<Image>();
            imageQuestion = Question.GetComponent<Image>();
            imageStatement = Statement.GetComponent<Image>();
        }
        private void Update()
        {
            switch(Tutorial_Manager.CurrPhase)
            {
                case 1:
                    ChangeColor(imageHome, blue, Color.white);
                    break;
                case 2:
                    ResetColor(imageHome, blue);
                    ChangeColor(imageTheme, blue, Color.white);
                    break;
                case 3:
                    ResetColor(imageTheme, blue);
                    ChangeColor(imageOptions, blue, Color.white);
                    break;
                case 4:
                    ResetColor(imageOptions, blue);
                    ChangeColor(imageCurr_Level, Color.black, lightblue);
                    break;
                case 5:
                    ResetColor(imageCurr_Level, Color.black);
                    if (ControllerPhase.playersExperiance % 25 > 12)
                    {
                        ChangeColor(imageProgess_Fill, lightblue, Color.white);
                    } else
                    {
                    ChangeColor(imageLevel_Progress, lightblue, Color.white);
                    }
                    break;
                case 6:
                    ResetColor(imageLevel_Progress, Color.white);
                    ResetColor(imageProgess_Fill, lightblue);
                    ChangeColor(imageNext_Level, Color.black, lightblue);
                    break;
                case 7:
                    ResetColor(imageNext_Level, Color.black);
                    ChangeColor(imageQuestion, transparent, Color.black);
                    break;
                case 8:
                    ResetColor(imageQuestion, transparent);
                    ChangeColor(imageStatement, transparent, Color.black);
                    break;
                case 9:
                    ResetColor(imageStatement, transparent);
                    break;
                    
            }
        }

        

        private static void ChangeColor(Image image, Color startColor, Color goalColor)
        {
            Color temp = new Color(image.color.r, image.color.g, image.color.b, image.color.a);
            float multilpikator = CalculateMultiplier(startColor, goalColor) * 2;

            //Change color values
            float redDiff = goalColor.r - startColor.r;
            float greenDiff = goalColor.g - startColor.g;
            float blueDiff = goalColor.b - startColor.b;
            float alphaDiff = goalColor.a - startColor.a;

            float lowest = CalculateLowest(redDiff, greenDiff, blueDiff);

            //Calculate value of change
            float redChangeValue = redDiff / lowest / 255f * multilpikator;
            float greenChangeValue = greenDiff / lowest / 255f * multilpikator;
            float blueChangeValue = blueDiff / lowest / 255f * multilpikator;
            float alphaChangeValue = alphaDiff / 255f * 8;

            
            //Change of increasing or decreasing of the color
            if (increase)
            {
                //Increase Color to goal

                //Case transparent
                if (startColor.a - goalColor.a != 0)
                {
                    temp.a += alphaChangeValue;
                    if (Math.Round(temp.a, 2) >= 1f)
                    {
                        temp.a = 1;
                        increase = false;
                    }
                //Case full color
                } else
                {
                    temp.r += redChangeValue;
                    temp.g += greenChangeValue;
                    temp.b += blueChangeValue;

                    if (temp.r >= goalColor.r) temp.r = goalColor.r;
                    if (temp.g >= goalColor.g) temp.g = goalColor.g;
                    if (temp.b >= goalColor.b) temp.b = goalColor.b;
                    CheckIncreaseChange(goalColor, temp);
                }

            }
            else
            {
                //Decrease Color back to start

                //Case transparent
                if (startColor.a - goalColor.a != 0)
                {
                    temp.a -= alphaChangeValue;
                    if (Math.Round(temp.a, 3) <= 0)
                    {
                        temp.a = 0f;
                        increase = true;
                    }
                //Case full color
                } else
                {
                    temp.r -= redChangeValue;
                    temp.g -= greenChangeValue;
                    temp.b -= blueChangeValue;

                    if (temp.r <= startColor.r) temp.r = startColor.r;
                    if (temp.g <= startColor.g) temp.g = startColor.g;
                    if (temp.b <= startColor.b) temp.b = startColor.b;
                    
                    CheckIncreaseChange(startColor, temp);
                }
                
            }
                    
            image.color = temp;
            

           

        }

        private static void CheckIncreaseChange(Color color, Color temp)
        {
            if (temp.r == color.r && temp.g == color.g && temp.b == color.b)
            {
                if (increase)
                {
                    increase = false;
                }
                else
                {
                    increase = true;
                }
            }
        }

        private static float CalculateLowest(float redDiff, float greenDiff, float blueDiff)
        {
            float lowest;
            if (redDiff <= greenDiff && redDiff <= blueDiff)
            {
                lowest = redDiff;
            }
            else if (greenDiff <= redDiff && greenDiff <= blueDiff)
            {
                lowest = greenDiff;
            }
            else
            {
                lowest = blueDiff;
            }

            return lowest;
        }

        private static void ResetColor(Image image, Color color)
        {
            image.color = color;
        }

        private void Awake()
        {
            imageHome = Home.GetComponent<Image>();
            imageTheme = Theme.GetComponent<Image>();
            imageOptions = Options.GetComponent<Image>();
            imageCurr_Level = Curr_Level.GetComponent<Image>();
            imageLevel_Progress = Level_Progress.GetComponent<Image>();
            imageProgess_Fill = Level_Progess_Fill.GetComponent<Image>();
            imageNext_Level = Next_Level.GetComponent<Image>();
            imageQuestion = Question.GetComponent<Image>();
            imageStatement = Statement.GetComponent<Image>();

        }

        private static Color ConvertColor(int r, int g, int b, int a)
        {
            return new Color(r / 255.0f, g / 255.0f, b / 255.0f, a / 255.0f);
        }

        private static float CalculateMultiplier(Color color1, Color color2)
        {
            float result = 0;
            result += color1.r - color2.r;
            result += color1.g - color2.g;
            result += color1.b - color2.b;
            return Math.Abs(result);
        }

        public void ResetBackground()
        {
            ResetColor(imageHome, blue);
            ResetColor(imageTheme, blue); ResetColor(imageOptions, blue);
            ResetColor(imageCurr_Level, Color.black);
            ResetColor(imageLevel_Progress, Color.white);
            ResetColor(imageProgess_Fill, lightblue);
            ResetColor(imageNext_Level, Color.black);
            ResetColor(imageQuestion, transparent);
            ResetColor(imageStatement, transparent);
        }
    }


}

