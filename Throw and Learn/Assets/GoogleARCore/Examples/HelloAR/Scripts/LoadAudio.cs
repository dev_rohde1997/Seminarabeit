﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GoogleARCore.Examples.HelloAR
{
    public class LoadAudio : MonoBehaviour {

        //Correct Answer Sound
        public AudioClip CorrectAnswerSound;
        

        //Wrong Answer Sound
        public AudioClip WrongAnswerSound;

        //Place Object Sound
        public AudioClip ObjectPlacedSound;

        //Ball Throwing Sound
        public AudioClip BallThrowingSound;

        //Level Up Sound
        public AudioClip LevelUpSound;

        //Audio Source
        public AudioSource AudioSource;
	
	    // Update is called once per frame
	    void Update () {
            //Auslöser
            if (AudioManager.SoundsActivce && UI_Manager.UI_is_active == false)
            {
                if (AudioManager.playCorrectAnswerSound)
                {
                    AudioSource.clip = CorrectAnswerSound;
                    AudioSource.Play();
                    AudioManager.playCorrectAnswerSound = false;
                } 

                if (AudioManager.playWrongAnswerSound)
                {
                    AudioSource.clip = WrongAnswerSound;
                    AudioSource.Play();
                    AudioManager.playWrongAnswerSound = false;
                }

                if (AudioManager.playObjectPlayedSound)
                {
                    AudioSource.clip = ObjectPlacedSound;
                    AudioSource.Play();
                    AudioManager.playObjectPlayedSound = false;
                }

                if (AudioManager.playBallThrownSound)
                {
                    AudioSource.clip = BallThrowingSound;
                    AudioSource.Play();
                    AudioManager.playBallThrownSound = false;
                }

                if (AudioManager.playLevelUpSound)
                {
                    AudioSource.clip = LevelUpSound;
                    AudioSource.Play();
                    AudioManager.playLevelUpSound = false;
                }
            }
           
        }

        
    }

}
